﻿from flask import Flask, render_template, request, url_for
import requests
import pickle
from bs4 import BeautifulSoup as BS
app = Flask(__name__)

cat = {"Поп": "https://krs.kassir.ru/bilety-na-koncert/estrada?sort=0&c=20",
       "Классика": "https://krs.kassir.ru/bilety-na-koncert/klassika?sort=0&c=20",
       "Рок": "https://krs.kassir.ru/bilety-na-koncert/rok?sort=0&c=20",
       "Электро": "https://krs.kassir.ru/bilety-na-koncert/electo?sort=0&c=20",
       "Хип-Хоп": "https://krs.kassir.ru/bilety-na-koncert/hip-hop?sort=0&c=20",
       "Драма": "https://krs.kassir.ru/bilety-v-teatr/dramaticheskij?sort=0&c=20",
       "Комедия": "https://krs.kassir.ru/bilety-v-teatr/komedii?sort=0&c=20",
       "Опера": "https://krs.kassir.ru/bilety-v-teatr/opera?sort=0&c=20",
       "Балет": "https://krs.kassir.ru/bilety-v-teatr/balet?sort=0&c=20",
       "Мюзикл": "https://krs.kassir.ru/bilety-v-teatr/myuzikl?sort=0&c=20",
       "Единоборства": "https://krs.kassir.ru/bilety-na-sportivnye-meropriyatiya/martial-arts?sort=0&c=20",
       "Игровые виды": "https://krs.kassir.ru/bilety-na-sportivnye-meropriyatiya/volleyball?sort=0&c=20",
       "Гимнастика": "https://krs.kassir.ru/bilety-na-sportivnye-meropriyatiya/gimnastika?sort=0&c=20",
       "Авто/мото спорт": "https://krs.kassir.ru/bilety-na-sportivnye-meropriyatiya/motosport?sort=0&c=20",
       "Зимние виды": "https://krs.kassir.ru/bilety-na-sportivnye-meropriyatiya/gornolyzhnyj-sport?sort=0&c=20",
       "Малышам": "https://krs.kassir.ru/detskaya-afisha/for-kids-2-6?sort=0&c=20",
       "7-12 лет": "https://krs.kassir.ru/detskaya-afisha/midage-7-12?sort=0&c=20",
       "От 12 лет": "https://krs.kassir.ru/detskaya-afisha/high-age-12?sort=0&c=20"}
with open('data.pickle', 'rb') as file:
    list = pickle.load(file)
print(list)

def parse(list):
    res_of_parse = ''
    for i in list:
        for j in cat:
            if str(i) == str(j):
                title = []
                date = []
                date2 = []
                place = []
                headers = {'user-agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'}
                page = requests.get(str(cat.get(j)), headers=headers)
                html = BS(page.content, "html.parser")
                p = html.findAll("div", "title")
                for res in p:
                    title.append(res.findNext('a').text)

                p = html.findAll("div", "date")
                for res in p:
                    date.append(res.text)
                date.pop(0)
                for k in date:
                    k = k[17:len(k)]
                    date2.append(k)
                date2.remove('')

                p = html.findAll("div", "place")
                for res in p:
                    place.append(res.text)

                size = len(place)
                for i in range(size):
                    event = "Событие: " + title[i] + ". Место: " + place[i] + ". Дата: " + date2[i]
                    res_of_parse = res_of_parse + event + ';'
    return res_of_parse

@app.route("/")
def index():
    url_for('static', filename='style.css')
    url_for('static', filename='style1280.css')
    url_for('static', filename='style1024.css')
    url_for('static', filename='script.js')
    return render_template('index.html', mail = 'Адрес эл. почты', login = 'Логин',
                           aumaillogin = 'Логин или пароль', aupassword = 'Пароль', username = 'Регистрация')


@app.route('/registration', methods=['GET', 'POST'])
def registration():
    list = []
    ch = 0
    string = str(request.form.get("email")) + ' ' + str(request.form.get("login"))\
             + ' ' + str(request.form.get("name")) + ' ' + str(request.form.get("password"))

    try:
        with open ('data.pickle', 'rb') as file:
            list = pickle.load(file)
    except:
        with open('data.pickle', 'wb') as file:
            pass
        file.close()

    if not list:
        with open('data.pickle', 'wb') as file:
            list.append(string)
            pickle.dump(list, file)
        file.close()
        print(list)
        return render_template('index.html', mail='Адрес эл. почты', login='Логин', aumaillogin = 'Логин или пароль',
                               aupassword = 'Пароль', username = 'Регистрация')
    else:
        for i in list:
            check = i.split(' ')
            if check[0] == str(request.form.get("email")):
                return render_template('index.html', prov = '1',  mail = 'Эл. почта занята!!!',
                                       login = 'Логин', aumaillogin = 'Логин или пароль', aupassword = 'Пароль', username = 'Регистрация')
            if check[1] == str(request.form.get("login")):
                return render_template('index.html', prov = '1',  mail = 'Эл. почта',
                                       login = 'Логин занят!!!', aumaillogin = 'Логин или пароль', aupassword = 'Пароль', username = 'Регистрация')

    with open('data.pickle', 'wb') as file:
        list.append(string)
        pickle.dump(list, file)
    file.close()
    print(list)

    return render_template('index.html', mail = 'Адрес эл. почты', login = 'Логин',
                           aumaillogin = 'Логин или пароль', aupassword = 'Пароль',username = 'Регистрация')


@app.route('/auth', methods=['GET', 'POST'])
def auth():
    string = str(request.form.get("loginoremail")) + ' ' + str(request.form.get("password"))
    print(string)
    user = string.split(' ')
    ch = 0

    try:
        with open ('data.pickle', 'rb') as file:
            list = pickle.load(file)
    except:
        return render_template('index.html', mail='Адрес эл. почты', login='Логин',
               aumaillogin='Неверный логин или пароль', aupassword = 'Пароль', prov = '2', username = 'Регистрация')

    for i in list:
        check = i.split(' ')
        print(check)
        if user[0] == check[0] or user[0] == check[1]:
            if user[1] == check[3]:
                return render_template('index.html', username = check[2], prov = '3')
            else:
                ch = 1
        else:
            ch = 2

    if ch == 1:
        return render_template('index.html', mail='Адрес эл. почты', login='Логин',
                               aumaillogin='Логин или пароль', aupassword='Неверный пароль', prov='2',
                               username='Регистрация')
    if ch == 2:
        return render_template('index.html', mail='Адрес эл. почты', login='Логин',
                               aumaillogin='Неверный логин или пароль', aupassword='Пароль', prov='2',
                               username='Регистрация')

    return render_template('index.html', mail='Адрес эл. почты', login='Логин', aumaillogin='Логин или пароль', aupassword = 'Пароль', username = 'Регистрация')


@app.route('/result', methods=['GET', 'POST'])
def result():
    list = str(request.form.get("list")).split(',')
    result = parse(list)
    print(result)
    return result

if __name__ == '__main__':
    app.run()
