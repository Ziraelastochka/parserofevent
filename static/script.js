let body1 = document.getElementById('header');//хэдер
let body2 = document.getElementById('main');//главная до входа
let body3 = document.getElementById('main2');//главная после входа
let body4 = document.getElementById('tags-window');//Окно с тегами
list_of_tags = document.getElementsByClassName("inp");
//Тут переменные данных пользователей
let tags = [];


let prov = document.getElementById('jsfromflask')

function registration_button()
{
  let window = document.getElementById('registration-window');
  body1.style.opacity = '0.1';
  body2.style.opacity = '0.1';
  window.style.display = 'block';
}

function close_registration_window()
{
  let window = document.getElementById('registration-window');
  body1.style.opacity = '1';
  body2.style.opacity = '1';
  window.style.display = 'none';  	
}

function enter_button()
{
  let window = document.getElementById('enter-window');
  body1.style.opacity = '0.1';
  body2.style.opacity = '0.1';
  window.style.display = 'block';

}

function close_enter_window()
{
  let window = document.getElementById('enter-window');
  body1.style.opacity = '1';
  body2.style.opacity = '1';
  window.style.display = 'none';  	
} 

function open_tags_window()
{
  body4.style.display = "block";
  body3.style.display = "none";
  let i;
  let j;
  for (i = 0; i < tags.length; i++)
  {
  	for (j = 0; j < list_of_tags.length; j++)
  	{
  	  if (tags[i] == list_of_tags[j].parentNode.textContent)
  	  {
  	  	list_of_tags[j].checked = true;
  	  }
  	}	
  } 	
}

function enter()
{
  let form1 = document.getElementById('enter-form');
  if (form1.children[0].value != '' && form1.children[1].value != '')
  { 

    body2.style.display = "none";
    body1.style.opacity = '1';
    a = document.getElementById('enter-button');
    b = document.getElementById('registration-button');
    b.remove();
    a.textContent = user_name;
    a.style.pointerEvents = 'none';
    a.style.marginLeft = '50%';
    body3.style.display = 'block';
  }
  else
  {
  	if (form1.children[0].value == '')
  	{
  	  form1.children[0].placeholder = 'Поле обязательно для заполнения';
  	}
  	if (form1.children[1].value == '')
  	{
  	  form1.children[1].placeholder = 'Поле обязательно для заполнения';	
  	}	
  }	
}

function save_tags()
{
  let i;
  for (i = 0; i < list_of_tags.length; i++)
  {
  	if (list_of_tags[i].checked)
  	{
  	  tags.push(list_of_tags[i].parentNode.textContent);
  	}	
  }
  body4.style.display = "none";
  body3.style.display = "block"; 
}
